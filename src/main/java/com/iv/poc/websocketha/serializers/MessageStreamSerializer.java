package com.iv.poc.websocketha.serializers;

import com.hazelcast.nio.serialization.ByteArraySerializer;
import com.iv.poc.websocketha.dtos.MessageDto;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.smile.SmileFactory;

import java.io.IOException;

public class MessageStreamSerializer implements ByteArraySerializer<MessageDto> {

    ObjectMapper mapper = new ObjectMapper(new SmileFactory());

    @Override
    public int getTypeId() {
        return 1;
    }

    @Override
    public byte[] write(MessageDto messageDto) throws IOException {
        return mapper.writeValueAsBytes(messageDto);
    }

    @Override
    public MessageDto read(byte[] bytes) throws IOException {
        return mapper.readValue(bytes, MessageDto.class);
    }

    @Override
    public void destroy() {
    }
}
