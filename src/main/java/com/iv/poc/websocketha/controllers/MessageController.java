package com.iv.poc.websocketha.controllers;

import com.hazelcast.core.HazelcastInstance;
import com.iv.poc.websocketha.dtos.MessageDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.UUID;

@Slf4j
@RestController()
@RequestMapping("/messages")
public class MessageController {

    private final HazelcastInstance hazelcastInstance;

    public MessageController(HazelcastInstance hazelcastInstance) {
        this.hazelcastInstance = hazelcastInstance;
    }

    @PostMapping
    public void send(@RequestBody MessageDto message) {
        log.debug("broadcasting message [message={}]", message);
        message.setId(UUID.randomUUID().toString());
        message.setDate(new Date().toString());
        hazelcastInstance.getTopic("message").publish(message);
    }

}
