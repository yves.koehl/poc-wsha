package com.iv.poc.websocketha.controllers;

import com.iv.poc.websocketha.managers.ConnexionManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.codec.ServerSentEvent;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;
import reactor.core.publisher.Flux;

import java.time.Duration;
import java.time.LocalTime;
import java.util.UUID;

@Slf4j
@RestController
@RequestMapping("/sse")
public class SSEStreamController {

    private final ConnexionManager connexionManager;

    public SSEStreamController(ConnexionManager connexionManager) {
        this.connexionManager = connexionManager;
    }

    @GetMapping(path = "/flux", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<String> streamFlux() {
        log.debug("streaming flux...");
        return Flux.interval(Duration.ofSeconds(1))
                .map(sequence -> {
                    log.debug("sending flux...");
                    return "Flux - " + LocalTime.now().toString();
                });
    }

    @GetMapping("/stream")
    public Flux<ServerSentEvent<String>> streamEvents() {
        log.debug("streaming sse...");
        return Flux.interval(Duration.ofSeconds(1))
                .map(sequence -> ServerSentEvent.<String>builder()
                        .id(String.valueOf(sequence))
                        .event("periodic-event")
                        .data("SSE - " + LocalTime.now().toString())
                        .build());
    }

    @GetMapping("/mvc")
    public SseEmitter streamSseMvc() {
        return connexionManager.createEmitter(UUID.randomUUID().toString());
    }
}
