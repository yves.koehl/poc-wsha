package com.iv.poc.websocketha.managers;

import com.iv.poc.websocketha.dtos.MessageDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Slf4j
public class ConnexionManager {

    private final Map<String, SseEmitter> emitters = new HashMap<>();

    /**
     * Create a new SSE emitter for a client id, and store it in internal list for push operations
     *
     * @param clientId the id of the client that initialize the SSE connection
     * @return initialized SSE emitter
     */
    public SseEmitter createEmitter(String clientId) {
        SseEmitter emitter = new SseEmitter();
        emitters.put(clientId, emitter);
        log.info("new emitter created [clientId={}, emittersCount={}", clientId, emitters.size());
        emitter.onCompletion(() -> {
            log.info("emitter completion [clientId={}]", clientId);
            emitters.remove(clientId);
        });
        emitter.onError(err -> {
            log.warn("emitter error [clientId={}]", clientId);
            log.warn("error", err);
            emitters.remove(clientId);
        });
        return emitter;
    }

    /**
     * Send message to all emitters
     *
     * @param message message to be sent
     */
    public void send(MessageDto message) {
        log.debug("sending message to {} emitters [message={}]", emitters.size(), message);

        SseEmitter.SseEventBuilder event = SseEmitter.event()
                .data(message)
                .id(message.getId())
                .name("sse event");

        ExecutorService sseMvcExecutor = Executors.newSingleThreadExecutor();
        sseMvcExecutor.execute(() -> {
            for (Map.Entry<String, SseEmitter> emitter : emitters.entrySet()) {
                try {
                    emitter.getValue().send(event);
                } catch (Exception ex) {
                    log.error("cannot send message to client {}", emitter.getKey());
                    log.error("error", ex);
                    emitter.getValue().completeWithError(ex);
                }
            }
        });
    }
}
