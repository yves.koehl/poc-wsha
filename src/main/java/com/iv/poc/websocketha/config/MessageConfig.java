package com.iv.poc.websocketha.config;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.ITopic;
import com.iv.poc.websocketha.dtos.MessageDto;
import com.iv.poc.websocketha.managers.ConnexionManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

@Slf4j
@Configuration
public class MessageConfig {

    private final HazelcastInstance hazelcastInstance;
    private final ConnexionManager connexionManager;

    public MessageConfig(HazelcastInstance hazelcastInstance, ConnexionManager connexionManager) {
        this.hazelcastInstance = hazelcastInstance;
        this.connexionManager = connexionManager;
    }

    @Bean
    public ITopic<MessageDto> messageTopic() {
        return hazelcastInstance.getTopic("message");
    }

    @PostConstruct
    public void init() {
        messageTopic().addMessageListener(message -> {
            log.info("new message from topic [message={}]", message);
            log.debug("broadcasting message...");
            connexionManager.send(message.getMessageObject());
        });
    }

}
