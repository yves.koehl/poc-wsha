package com.iv.poc.websocketha.config;

import com.iv.poc.websocketha.managers.ConnexionManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class AppConfig {

    @Bean
    @Scope("singleton")
    public ConnexionManager connexionManager() {
        return new ConnexionManager();
    }

}
