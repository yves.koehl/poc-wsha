Swarm integration lib and documentation: `https://github.com/bitsofinfo/hazelcast-docker-swarm-discovery-spi`  
Use DNSRR  
Use SSE instead of WebSocket for now (to be implemented)  

# Warning
Swarm integration library doesn't work with hazelcast 4.x+, changes have been done on structure  
See https://docs.hazelcast.org/docs/rn/index.html for more details  
Fork or pull request has to be done or use hazelcast 3.x (currently tested with 3.12 version)  

# Configure
Update `resources/application.properties` and `resources/hazelcast-<env>.xml`  

# Build
`./mvnw package`: build jar package  
`docker build -t websocketha-service .`: build docker image

# Deploy on Swarm cluster
`docker network create -d overlay websocketha`: create an overlay network (first time only)  
`docker service create --network websocketha --name websocketha-service --endpoint-mode dnsrr --replicas 2 -p target=8080,protocol=tcp,mode=host websocketha-service java -jar /app.jar`: start service with 2 replicas  
`docker service rm websocketha-service`: stop service  
